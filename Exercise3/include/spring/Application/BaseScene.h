#pragma once

#include <spring\Framework\IScene.h>
#include <qobject.h>



namespace Spring
{
	class BaseScene : public IScene
	{
		Q_OBJECT

	public:
		BaseScene(const std::string& ac_szSceneName);

		void createScene() override;

		void release() override;

		~BaseScene();

		public slots:
			void mf_previousButton();
			void mf_nextButton();


	private:
		QWidget * centralWidget;

	};
}
