#include <spring/Application/InitialScene.h>

#include "ui_hello_world.h"
namespace Spring
{

	InitialScene::InitialScene(const std::string& ac_szSceneName) : IScene(ac_szSceneName)
	{
	}

	void InitialScene::createScene()
	{
		auto ui = std::make_unique<Ui_MainWindow>();
		ui->setupUi(m_uMainWindow.get());
	}

	void InitialScene::release()
	{
		delete centralWidget;
	}

	InitialScene::~InitialScene()
	{

	}
	
}
