#include <spring/Application/InitialScene.h>

#include "ui_say_hello.h"
namespace Spring
{

	InitialScene::InitialScene(const std::string& ac_szSceneName) : IScene(ac_szSceneName)
	{
	}

	void InitialScene::createScene()
	{
		//create the UI
		auto ui = std::make_unique<Ui_MainWindow>();
		ui->setupUi(m_uMainWindow.get());

		//connect btn's release signal to defined slot
		QObject::connect(ui->pushButton, SIGNAL(released()), this, SLOT(mf_OkButton()));

		//set centralWidget
		centralWidget = ui->centralwidget;
	}

	void InitialScene::release()
	{
		delete centralWidget;
	}

	InitialScene::~InitialScene()
	{

	}

	void InitialScene::mf_OkButton()
	{
		auto nameLineEdit = centralWidget->findChild<QLineEdit*>("lineEdit");
		const std::string appName = nameLineEdit->text().toStdString();

		if (!appName.empty())
		{
			m_TransientDataCollection.erase("Name");
			m_TransientDataCollection.emplace("Name", appName);
		}

		const std::string c_szNextSceneName = "Hello scene";
		emit SceneChange(c_szNextSceneName);
	}
	
}
