#include <spring/Application/BaseScene.h>

#include "ui_window_count.h"
namespace Spring
{

	BaseScene::BaseScene(const std::string& ac_szSceneName) : IScene(ac_szSceneName)
	{
	}

	void BaseScene::createScene()
	{
		auto ui = std::make_unique<Ui_MainWindow>();
		ui->setupUi(m_uMainWindow.get());;
		QObject::connect(ui->pushButtonNext, SIGNAL(released()), this, SLOT(mf_nextButton()));
		QObject::connect(ui->pushButtonPrevious, SIGNAL(released()), this, SLOT(mf_previousButton()));

		centralWidget = ui->centralwidget;
		int transientNumber = boost::any_cast<int>(m_TransientDataCollection["Number"]);
		m_uMainWindow->setWindowTitle(QString("window index in array: ") + QString::number(499 + transientNumber));
		ui->label->setText(QString::number(transientNumber));
		
	}

	void BaseScene::release()
	{
		delete centralWidget;
	}

	BaseScene::~BaseScene()
	{

	}

	void BaseScene::mf_previousButton()
	{
		int currentCount = boost::any_cast<int>(m_TransientDataCollection["Number"]);

		currentCount--;

		if (!m_TransientDataCollection.empty())
		{
			m_TransientDataCollection.erase("Number");
		}
		m_TransientDataCollection.emplace("Number", currentCount);

		const std::string c_szNextSceneName = "scene_" + (499 + currentCount);
		emit SceneChange(c_szNextSceneName);
	}

	void BaseScene::mf_nextButton()
	{
		int currentCount = boost::any_cast<int>(m_TransientDataCollection["Number"]);

		currentCount++;

		if (!m_TransientDataCollection.empty())
		{
			m_TransientDataCollection.erase("Number");
		}
		m_TransientDataCollection.emplace("Number", currentCount);

		const std::string c_szNextSceneName = "scene_" + (499+ currentCount);
		emit SceneChange(c_szNextSceneName);
	}

}
