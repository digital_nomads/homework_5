#include <QApplication.h>
#include <spring\Framework\Application.h>
#include <spring\Application\ApplicationModel.h>


int main(int argc, char **argv)
{
	QApplication qApplication(argc, argv);

	Spring::Application& app = Spring::Application::getInstance();

	Spring::ApplicationModel appModel;

	app.setApplicationModel(&appModel);

	app.start("Spring school project", 420, 420);

	qApplication.exec();

	return 0;
}