#include <spring/Application/InitialScene.h>

#include "ui_window_count.h"
namespace Spring
{

	InitialScene::InitialScene(const std::string& ac_szSceneName) : IScene(ac_szSceneName)
	{
	}

	void InitialScene::createScene()
	{
	}

	void InitialScene::release()
	{
		delete centralWidget;
	}

	InitialScene::~InitialScene()
	{

	}
}
