#pragma once
#include <spring\Framework\IApplicationModel.h>
#include <spring\Application\global.h>
#include <spring\Application\InitialScene.h>
#include <spring\Application\BaseScene.h>

namespace Spring
{
	class Application_EXPORT_IMPORT_API ApplicationModel : public IApplicationModel
	{
	public:

		ApplicationModel();

		virtual void defineScene();

		virtual void defineInitialScene();

		virtual void defineTransientData();

	private:
		std::vector<std::shared_ptr<BaseScene>> m_sceneArray;
	};
}
