#include <spring\Application\BaseScene.h>

#include "ui_base_scene.h"

namespace Spring
{
	BaseScene::BaseScene(const std::string& ac_szSceneName) : IScene(ac_szSceneName)
	{
	}
	void BaseScene::createScene()
	{
		//create the UI
		const auto ui = std::make_shared<Ui_baseScene>();
		ui->setupUi(m_uMainWindow.get());

		//setting a temporary new title
		m_uMainWindow->setWindowTitle(QString("Hello to you"));

		//setting centralWidget
		centralWidget = ui->centralwidget;

		//Get the title from transient data
		std::string name = boost::any_cast<std::string>(m_TransientDataCollection["Name"]);

		ui->label->setText(QString::fromStdString("Hello " + name));

	}
	void BaseScene::release()
	{
		delete centralWidget;
	}

	BaseScene::~BaseScene()
	{

	}
}
