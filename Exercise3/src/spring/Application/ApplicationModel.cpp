#include "..\..\..\include\spring\Application\ApplicationModel.h"
#include <spring\Application\BaseScene.h>


namespace Spring
{
	ApplicationModel::ApplicationModel():IApplicationModel()
	{

	}

	void ApplicationModel::defineScene()
	{

		for (int i = 0; i < 1000; ++i)
		{
			const std::string sceneName = "scene_" + i;
			std::shared_ptr<BaseScene> currentScene = std::make_shared<BaseScene>(sceneName);
			m_sceneArray.push_back(currentScene);
			m_Scenes.emplace(sceneName, currentScene.get());
		}
	}
	void ApplicationModel::defineInitialScene()
	{
		mv_szInitialScene = "scene_499";
	}
	void ApplicationModel::defineTransientData()
	{
		m_TransientData.emplace("Number", 0);
	}
}
